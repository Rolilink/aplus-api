var q = require('q')
		_ = require('underscore')
		request = require('request')
		cheerio = require('cheerio');

var getPerfil = function(body){
	var jsSession = /perfil\('(.*)'\)/;
	if(jsSession.test(body))
		return jsSession.exec(body)[1];
	$ = cheerio.load(body);
	return $('img[name="Image1"]').parent().attr("href").split("'")[1];

};
/**
 * First Step for authentication, scrape __VIEWSTATE from opts.url 
 */
var getViewState = function(){
	var deferred = q.defer(),
			args = Array.prototype.slice.call(arguments),
			opts = args[0],
			$viewstate = opts.viewstate || 'input[name="__VIEWSTATE"]',
			reqOpts = _.omit(opts,'__VIEWSTATE'),
			cookies = request.jar(),
			$ = null,
			form = null;
	
	reqOpts = _.defaults(reqOpts,{url:'https://matricula.utp.ac.pa', strictSSL:false,jar:cookies});
	request.get(reqOpts,function(err,response,body){

		if(err)
			return deferred.reject(err);
		
		$	= cheerio.load(body);

		if($){
			form = {
				txtCedula: reqOpts.cedula,
				txtClave: reqOpts.clave,
				__VIEWSTATE:$($viewstate).val(),
				__EVENTTARGET:"",
				__EVENTARGUMENT:"",
				"imgbEnviar.x":"74",
				"imgbEnviar.y":"13"
			};
			
			deferred.resolve({form:form,reqOpts:_.pick(reqOpts,"strictSSL","jar")});
		
		}else{
			return deferred.reject(new Error("cheerio cant load body"));
		}
	});
	
	return deferred.promise;
};


/**
 * Post credentials to acceso.aspx for authenticating the user
 */
var postCredentials = function(){
	var deferred = q.defer(),
			args = Array.prototype.slice.call(arguments),
			opts = args[0],
			$ = null,
			reqOpts = _.extend(opts.reqOpts,{url:"https://matricula.utp.ac.pa/acceso.aspx",form:opts.form});

	request.post(reqOpts,function(err,response,body){
		if(err)
			return deferred.reject(err);

		$ = cheerio.load(body);
		if($){
			return deferred.resolve(_.pick(reqOpts,'strictSSL','jar'));
		}else{
			return deferred.reject(new Error("cheerio cant load body"));
		}
	});

	return deferred.promise;
};


/**
 * Validates the user and returns a profile id for configuring the user
 */
var validateUser = function(){
	var deferred = q.defer(),
			args = Array.prototype.slice.call(arguments),
			opts = args[0],
			reqOpts = _.extend(opts,{url:"https://matricula.utp.ac.pa/validate_user.asp"}),
			form = {},
			$ = null,
			form = null;

	request.get(reqOpts,function(err,response,body){
		if(err)
			deferred.reject(err)

		$ = cheerio.load(body);

		if($){
			if($('input[name="__VIEWSTATE"]').length > 0)
				return deferred.reject(new Error("cedula o clave incorrectos"));

			form = {
				perf: getPerfil(body),
				cedula: ""
			};
			return deferred.resolve({form:form,reqOpts:_.pick(reqOpts,"strictSSL","jar")}); 
		}else{
			return deferred.reject(new Error("cheerio cant load body"));
		}
	});
	return deferred.promise;
};


/**
 * Take a profile id and validates the user
 */
var configureUser = function(){
	var deferred = q.defer(),
			args = Array.prototype.slice.call(arguments),
			opts = args[0],
			reqOpts = _.extend(opts.reqOpts, {url:"https://matricula.utp.ac.pa/config_user.asp", form:opts.form});
	request.post(reqOpts,function(err,response,body){
		if(err)
			return deferred.reject(err);
		return deferred.resolve(_.pick(reqOpts,"strictSSL","jar"))
	});
	return deferred.promise;
};


/**
 * Create the session on the matricula system and return the set of cookies
 */
var logIn = function(){
	var deferred = q.defer(),
			args = Array.prototype.slice.call(arguments),
			opts = args[0],
			reqOpts = _.extend(opts,{url:"https://matricula.utp.ac.pa/estudia/Bienvenida.aspx"})
			$ = null
			session = {};
	request.get(reqOpts,function(err,response,body){
		if(err)
			deferred.reject(err);

		$ = cheerio.load(body);

		if(!$)
			return deferred.reject(new Error("cheerio cant load body"));

		session.nombre = $('#lblnombre').text().split(',')[1];
		session.apellido = $('#lblnombre').text().split(',')[0];
		session.year = $('#lblAnoEstudio').text();
		session.carrera = $('#lblCarrera').text();
		session.indice = $('#lblIndice b').text();
		session.cookies = reqOpts.jar;
		return deferred.resolve(session);
	});
	return deferred.promise;
};

/**
 * Function that the module exposes to make auth
 * @param  {[String]}   cedula   [The user identification number]
 * @param  {[type]}   password [The user password]
 * @param  {Function} cb       [a callback that takes session cookies as arg]
 */
var auth = function(cedula, password, cb , errCb){
	getViewState({cedula:cedula,clave:password})
		.then(postCredentials)
		.then(validateUser)
		.then(configureUser)
		.then(logIn)
		.then(cb)
		.catch(errCb);
};

module.exports.auth = auth;