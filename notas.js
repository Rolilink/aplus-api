var q = require('q')
		_ = require('underscore')
		request = require('request')
		cheerio = require('cheerio')
		iconv = require('iconv');

var getNotas = function(cookies){
	var deferred = q.defer(),
			aSemester = "",
			aYear = "",
			notas = [],
			$ = null,
			htmlData = "",
			ic = new iconv.Iconv('iso-8859-1', 'utf-8'),
			htmlBuffer = null,
			body = "";

	request.get({url:"https://matricula.utp.ac.pa/estudia/historial_notas.asp", strictSSL:false,jar:cookies,encoding:null},function(error,response,buffer){
			
			if(error)
				return deferred.reject(error);
			
			htmlBuffer = ic.convert(buffer);
			body = htmlBuffer.toString('utf-8');

			$ = cheerio.load(body);
			
			if(!$)
				return deferred.reject(new Error("cheerio cant load body"));
			
			htmlData = $('table[bgcolor="gray"] tr');
			htmlData.each(function(i,el){
				var res;
				if($(el).hasClass('header')){
					res = $(el).children().map(function(index,td){
						return $(td).text();
					});
					aSemester = res[1];
					aYear = res[0];
				}

				if($(el).attr('bgcolor') === '#ffffe0'){
					var res;
					res = $(el).children().map(function(index,td){
						return $(td).html();
					});
					notas.push({
						name: res[0],
						codasig: res[1],
						codhora: res[2],
						value: res[3],
						valuesem: res[4],
						semester: aSemester,
						year: aYear
					});
				}

			});
			return deferred.resolve(notas);
	});
	return deferred.promise;
}

module.exports.getNotas = getNotas;